# VidLink
VidLink is an expandable platform for spreading videos over multiple services.

## Details
VidLink will provide a way to upload a video once and publish on many other major services. It will be expandable with plugins.
This project is not in an usable state at the moment.

## Roadmap
 - Create plugin loading framewok :white_check_mark:
 - Create plugin api
 - Load plugins from plugin folder.
 - Config framework
 - Create CLI as frontend
 - Create REST Api
 - Create HTML5 Web Interface
 - Create Frontend plugin api

## Contributing
More information on how to contribute to this project can be found under [CONTRIBUTING](CONTRIBUTING.md). Please also refer to the [CODE OF CONDUCT](CODE_OF_CONDUCT.md) file.

## License
This project is licensed under the Apache License 2.0. You can find more information about it in the [LICENSE](LICENSE) file.
